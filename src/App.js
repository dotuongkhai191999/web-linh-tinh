import "./App.css";
import {
    BrowserRouter,
    Routes,
    Route,
} from "react-router-dom";
import React from 'react'
import store from "./Component/Redux/Store.js";
import { Provider as StoreProvider } from 'react-redux';
import { routes } from "./Routes";

function App() {
    const routeElement =
        routes.map((route, index) => {
            return <Route key={index} path={route.path} exact={route.exact} element={route.component} />
        })


    return <StoreProvider store={store}>
        <BrowserRouter>
            <Routes>
                {routeElement}
            </Routes>
        </BrowserRouter>
    </StoreProvider>

}

export default App;
