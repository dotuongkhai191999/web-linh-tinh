import axios from "axios";
const onCallAPI = (method, url, data = {}, params = {}, headers = {}) => {
    const domain = 'http://api.com/' //domain hosting of api
    //method POST, GET, PUT, DELETE, ...
    // URL: THE LINK DIRECT TO API AFTER CUT THIS.DOMAIN
    //data: Image, video, ....
    // params: object condition send to server 
    // header 
    return axios({
        method: method,
        url: domain + url,
        data: data,
        params: params,
        headers: headers,
    });
};
export default onCallAPI;
