import React, { useState, useLayoutEffect, useEffect } from 'react'
import { useNavigate } from 'react-router';
import * as myconst from '../Constants'
export default function HomePage() {
    const navigate = useNavigate();
    const [IsLogin, setIsLogin] = useState();
    useLayoutEffect(() => {
        setIsLogin(localStorage.getItem(myconst.IsLogin)?.length ? true : false)
    }, [IsLogin])

    useEffect(() => {
        if (IsLogin === false) navigate("/login", { replace: true });
    }, [IsLogin, navigate])
    const signOut = () => {
        localStorage.clear()
        navigate("/login", { replace: true });
    }

    return (
        <div>HomePage
            <button type="button"
                id="sign-out-btn"
                onClick={signOut}
                className="btn btn-primary btn-lg btn-block">
                Đăng xuất</button>
        </div>
    )
}
