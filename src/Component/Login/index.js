import React, { useState, useLayoutEffect, useEffect, } from 'react'
import * as myconst from '../Constants'
import { useNavigate } from "react-router-dom";
import './index.css'
import onCallAPI from '../API';
export default function Login() {
    const navigate = useNavigate();
    const [IsLogin, setIsLogin] = useState();
    const [seePass, setSeePass] = useState(false);
    const [logingText, setLogingText] = useState({
        userName: "",
        password: ""
    });
    const [visibleClear, setvisibleClear] = useState({
        userName: false,
        password: false
    });
    const changeText = (value, text) => {
        if (text === "name") {
            setLogingText({ ...logingText, userName: value });
            setvisibleClear({ ...visibleClear, userName: true });
        }
        else {
            setLogingText({ ...logingText, password: value })
            setvisibleClear({ ...visibleClear, password: true });
        }
        if (value === "") {
            setvisibleClear({
                userName: false,
                password: false
            })
        }
    }
    const otherClick = () => {
        setvisibleClear({ userName: false, password: false })
    }

    const clearButton = (text) => {
        if (text === "name") {
            setLogingText({ ...logingText, userName: "" });
            document.getElementById("username-text").focus();
            setvisibleClear({ ...visibleClear, userName: false });
        } else {
            setLogingText({ ...logingText, password: "" })
            setvisibleClear({ ...visibleClear, password: false });
            document.getElementById("password-text").focus();
        }
    }
    const clickLogin = async () => {
        if (logingText.userName.trim() === "") {
            console.log(logingText);
            alert("Bạn cần nhập tài khoản để đăng nhập!!!");
            setIsLogin(false);
        }
        else if (logingText.password.trim() === "") {
            alert("Bạn cần nhập mật khẩu để đăng nhập!!!");
            setIsLogin(false);
        }
        else {
            // const result = await onCallAPI('post', "/login", {}, logingText, {});
            // if (!result.data.data) {
            //     alert("Bạn đã nhập sai tài khoản/mật khẩu, vui lòng thử lại!!!")
            //     return;
            // }
            localStorage.setItem(myconst.IsLogin, true)
            localStorage.setItem(myconst.user_token, logingText.userName + logingText.password + "123456a")
            setIsLogin(true);
        }
        return;
    }

    useLayoutEffect(() => {
        setIsLogin(localStorage.getItem(myconst.IsLogin)?.length ? true : false)
    }, [IsLogin])

    useEffect(() => {
        // console.log(IsLogin)
        if (IsLogin === true) navigate("/", { replace: true });
    }, [IsLogin, navigate])

    const keydownLogin = (e) => {
        if (e.key === 'Enter') {
            clickLogin();
        }
        if (e.key === 'Tab') {
            setvisibleClear({ userName: false, password: false });
        }
    }
    return (
        <div className="App App-header" onClick={otherClick}>
            <div className='division-Login'>
                <p style={{ fontWeight: "700", color: "white" }}>Đăng nhập</p>
                <div className="flex1 division1" onClick={otherClick}>
                    <div className="flex1 mx-2 division-text">
                        <div className="flex1 division-input">
                            <i className="fas fa-user description-input"></i>
                            <input autoComplete="off" className="flex8 btn-primary input-text" value={logingText.userName}
                                onKeyDown={(e) => keydownLogin(e)}
                                autoFocus onChange={({ target }) => changeText(target.value, "name")} id="username-text" />
                            <button type="button" style={{ display: visibleClear.userName ? "flex" : "none", flex: 1 }} id="btn-clear-name"
                                className="btn btn-clear-text" onClick={() => clearButton("name")}
                                tabIndex={1000}
                            >
                                <i className="fa fa-times-circle " style={{ fontSize: "1.5em", }} ></i>
                            </button>
                        </div>
                        <div className="flex1 division-input" >
                            <i className="fas fa-key description-input"></i>
                            <input className="flex1 btn-primary input-text" value={logingText.password} type={seePass ? "text" : "password"}
                                onKeyDown={(e) => keydownLogin(e)}
                                onChange={({ target }) => changeText(target.value, "password")}
                                id="password-text" />
                            <button type="button" id="btn-clear-pass" tabIndex={"1001"}
                                style={{ display: visibleClear.password ? "flex" : "none", flex: 1 }} onClick={() => clearButton("password")} className="btn btn-clear-text">
                                <i className="fa fa-times-circle " style={{ fontSize: "1.5em", }} ></i>
                            </button>
                        </div>
                    </div>
                    <button type="button" id="btnLogin" className="flex2 btn btn-primary" onClick={() => clickLogin()}>Đăng nhập</button>
                </div>
                <label className="form-check-label flex1 justify-content-center nav-class check-box-login">
                    <input type="checkbox" className="form-check-input" id="see-password"
                        onChange={() => setSeePass(!seePass)}
                        value="checkedValue" checked={seePass} />
                    &nbsp;Hiển thị mật khẩu
                </label>
                <nav className="nav-class flex1 nav justify-content-center">
                    <a className="nav-link active" href="/forgot-password">Quên mật khẩu</a>
                    <a className="nav-link active" href="/sign-up">Đăng ký</a>
                </nav>
            </div>
        </div >
    );
}
