import React from 'react'
import './index.css'
import { useLocation } from 'react-router-dom'
export default function Notfound() {
    const location = useLocation();
    console.log(location);
    return (
        <div className='body-notfound container flex-1'>
            <div className='paragraph-not-found'>
                <p><b>404.</b> <ins>That's an error.</ins></p>
                <p>The requested URL&nbsp;<b style={{ color: "red" }}>{location.pathname}</b>&nbsp;was not found on this server.&nbsp;<ins>That's all we know.</ins></p>
                <img src="//www.google.com/images/errors/robot.png"
                    className="robot-error"
                    alt="robot-error" />
            </div>
        </div>
    )
}
