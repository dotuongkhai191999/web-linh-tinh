import React, { useEffect, useLayoutEffect, useState } from 'react'
import { useNavigate } from 'react-router';
import * as myconst from '../Constants'
import './index.css'

export default function Password() {
    const [IsLogin, setIsLogin] = useState();
    const [confirmPassword, setConfirmPassword] = useState("")
    const [seePass, setSeePass] = useState(false)
    const navigate = useNavigate();
    useLayoutEffect(() => {
        setIsLogin(localStorage.getItem(myconst.IsLogin)?.length ? true : false)
    }, [IsLogin])

    useEffect(() => {
        if (IsLogin === false) navigate("/login", { replace: true });
    }, [IsLogin, navigate])
    const [informationUser, setInformationUser] = useState({
        username: "", oldPassword: "", newPassword: ""
    })

    return (
        <div>{seePass?.toString()}</div>
    )
}
