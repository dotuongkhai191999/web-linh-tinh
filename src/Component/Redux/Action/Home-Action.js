import * as types from "../Constants.ActionType";
// importp onCallAPI from '../../API'
export const Login_Request = (loginInfo) => {
    // comment = {username, user avatar, userId, cmt_content}
    return async (dispatch) => {
        try {
            if (Object.keys(loginInfo).length !== 0) {
                dispatch({ type: types.Login_Success, data: loginInfo });
                // onCallAPI("post", "user/login", {}, { userid: userid, password: password }, {}).then(res => { //mean after call api and receive a value from server (data or error)
                //     dispatch({ type: types.Login_Success, data: res.data }); //when success, fe will receive an object contains token user or info for after login do
                // }).catch((err) => console.log(err)); // do something if error existing also dispatch action err 
            }
            else dispatch({ type: types.Login_Failed, err: "User and password is required" });
        } catch (err) {
            dispatch({ type: types.Login_Failed, err: err });
        }
    };
};
export const Logout_Request = (token) => {
    // comment = {username, user avatar, userId, cmt_content}
    return async (dispatch) => {
        try {

            //token for call api logout to clear token on server side
            dispatch({ type: types.Logout_Success });
            // onCallAPI("post", "user/login", {}, { userid: userid, password: password }, {}).then(res => { //mean after call api and receive a value from server (data or error)
            //     dispatch({ type: types.Login_Success, data: res.data }); //when success, fe will receive an object contains token user or info for after login do
            // }).catch((err) => dispatch({ type: types.Logout_Failed, err: err });); // do something if error existing also dispatch action err 
        } catch (err) {
            dispatch({ type: types.Login_Failed, err: err });
        }
    };
};
