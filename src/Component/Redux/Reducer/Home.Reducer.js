import * as types from "../Constants.ActionType";
var initState = {
    token: "",
    err_code: "",
};
var HomeReducer = (state = initState, action) => {
    switch (action.type) {
        case types.Login_Success: {
            const { data } = action;
            const token = data.userName + data.password + "123456";
            console.log(token)
            return { ...state, err_code: "", token: token };
        }
        case types.Login_Failed: {
            const { err } = action;
            console.log(err)
            return { ...state, err_code: err }
        }
        case types.Logout_Success: {
            return initState;
        }
        default:
            return state;
    }
};
export default HomeReducer;
