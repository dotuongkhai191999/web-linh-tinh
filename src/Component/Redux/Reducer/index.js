import { combineReducers } from "redux";
import HomeReducer from "./Home.Reducer";
const myReducer = combineReducers({
    stateHome: HomeReducer,
});
export default myReducer;
