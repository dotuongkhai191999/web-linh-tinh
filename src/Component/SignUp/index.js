import React, { useEffect, useLayoutEffect, useState } from 'react'
import { useNavigate } from 'react-router';
import * as myconst from '../Constants'
import './index.css'

export default function Password() {
    const navigate = useNavigate();
    const [signupInfo, setSignupInfo] = useState({
        userName: "", password: "", email: "", address: "", roleId: "", phone: "", fullName: ""
    });
    const [IsLogin, setIsLogin] = useState(false);
    useLayoutEffect(() => {
        setIsLogin(localStorage.getItem(myconst.IsLogin)?.length ? true : false)
    }, [IsLogin])
    useEffect(() => {
        document.getElementById("nut").click();
    }, [])

    useEffect(() => {
        if (IsLogin === true) navigate("/", { replace: true });
    }, [IsLogin, navigate])
    return (
        <>
            <button type="button" className="btn btn-primary" id="nut" data-bs-toggle="modal" data-bs-target="#exampleModal">
                Launch demo modal
            </button>

            <div className="modal fade" id="exampleModal" tabIndex={"-1"} aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">Thông tin tài khoản</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            <div className="input-focus-effect">
                                <input type="text" placeholder=" " />
                                <label>Họ và tên</label>
                            </div>
                            <div className="input-focus-effect">
                                <input type="text" placeholder=" " />
                                <label>Email</label>
                            </div>
                            <div className="input-focus-effect">
                                <input type="text" placeholder=" " />
                                <label>Số điện thoại</label>
                            </div>
                            <div className="input-focus-effect">
                                <input type="text" placeholder=" " />
                                <label>Tài khoản</label>
                            </div>
                            <div className="input-focus-effect">
                                <input type="text" placeholder=" " />
                                <label>Mật khẩu</label>
                            </div>
                            <div className='input-radio'>
                                <input type="radio" id="male" className="radio-input" name="gender" />
                                <label for="male" className="radio-label"></label>
                                <input type="radio" id="fmale" className="radio-input" name="gender" />
                                <label for="fmale" className="radio-label"></label>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="button" className="btn btn-primary">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
        </>

    )
}
