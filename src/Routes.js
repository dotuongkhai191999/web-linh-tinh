import SignUp from './Component/SignUp'
import HomePage from './Component/HomePage'
import Password from './Component/Password'
import Login from './Component/Login'
import Notfound from './Component/Notfound';

export const routes = [{
    path: "/",
    exact: true,
    component: <HomePage />,
}, {
    path: "/home",
    component: <HomePage />,
}, {
    path: "/login",
    component: <Login />,
}, {
    path: "/password",
    component: <Password />,
}, {
    path: "/forget-password",
    component: <Password />,
}, {
    path: "/forgot-password",
    component: <Password />,
}, {
    path: "/sign-up",
    component: <SignUp />,
}, {
    path: "*",
    component: <Notfound />,
},]